import pygame
import sys
from pygame.sprite import Sprite

class Ball(Sprite):
    def __init__(self, screen, direction):
        super(Ball, self).__init__()
        self.size = 10
        self.rect = pygame.Rect(160, 10, self.size, self.size)
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.direction = 1
        self.vdirection = 1
        self.speed = 2
        

    def update(self):
        if self.direction == 1:
            self.rect.left += int(self.speed)
        elif self.direction == 0:
            self.rect.left -= int(self.speed)
        if self.vdirection == 1:
            self.rect.top += int(self.speed)
        if self.vdirection == 0:
            self.rect.top -= int(self.speed)

    def draw_ball(self):
        pygame.draw.rect(self.screen, (100, 10, 10), self.rect)

class Paddle(Sprite):
   
    def __init__(self, screen, box_width, box_height, box_color, l):
        super(Paddle, self).__init__()
        self.screen = screen
        self.rect = pygame.Rect(0,0, box_width, box_height)
        self.screen_rect = screen.get_rect()
        self.rect.top = self.screen_rect.bottom - box_height
        self.moving_up = 0
        self.moving_down = 0
        self.box_height = box_height
        self.rect.left = l

    def update(self):
        if self.moving_up == 1:
            if self.rect.top > self.screen_rect.top:
                self.rect.top -= 5
        elif self.moving_down == 1:
            if self.rect.bottom < self.screen_rect.bottom:
                self.rect.top += 5
            
    def draw_paddle(self):
        pygame.draw.rect(self.screen, (100, 10, 10), self.rect)


def run_game():
    pygame.init()
    clock = pygame.time.Clock()
    width = 320
    height = 200
    box_width = 20
    box_height = 40
    bg_color = (0, 100, 0)
    box_color = (80, 10, 10)
    screen = pygame.display.set_mode((width, height))

    myfont = pygame.font.SysFont("Terminal", 96)
    p1_score = 0
    p2_score = 0
    label1 = myfont.render(str(p1_score), 1, (255,255,0))
    label2 = myfont.render(str(p2_score), 1, (255,255,0))
    
    screen.fill(bg_color)
    screen.blit(label1, (110, 100))
    screen.blit(label2, (210, 100))

    p1 = Paddle(screen, box_width, box_height, box_color, 0)
    p2 = Paddle(screen, box_width, box_height, box_color, 300)
    p2.draw_paddle()
    p1.draw_paddle()
    ball = Ball(screen, 1)
    ball.draw_ball()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    sys.exit()
                if event.key == pygame.K_a:
                    p1.moving_up = 1
                if event.key == pygame.K_z:
                    p1.moving_down = 1
                if event.key == pygame.K_UP:
                    p2.moving_up = 1
                if event.key == pygame.K_DOWN:
                    p2.moving_down = 1
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_a:
                    p1.moving_up = 0
                if event.key == pygame.K_z:
                    p1.moving_down = 0
                if event.key == pygame.K_UP:
                    p2.moving_up = 0
                if event.key == pygame.K_DOWN:
                    p2.moving_down = 0
        
        if ball.rect.colliderect(p2.rect):
            ball.direction = 0
            ball.speed = ball.speed * 1.01
        if ball.rect.colliderect(p1.rect):
            ball.direction = 1
            ball.speed = ball.speed * 1.01

        if ball.rect.top >= height - ball.size:
            ball.vdirection = 0
        if ball.rect.top <= 0:
            ball.vdirection = 1

        if ball.rect.left <= 0:
            ball.rect.left = 160
            p2_score += 1
        if ball.rect.left >= width:
            ball.rect.left = 160
            p1_score += 1
                
        screen.fill(bg_color)
        label1 = myfont.render(str(p1_score), 1, (255,255,0))
        label2 = myfont.render(str(p2_score), 1, (255,255,0))
        screen.blit(label1, (int(width * .25)-16, 100))
        screen.blit(label2, (int(width * .75)-16, 100))

        ball.update()
        ball.draw_ball()
        p1.update()
        p1.draw_paddle()
        p2.update()
        p2.draw_paddle()
        pygame.display.flip()
        clock.tick(50)

run_game()



